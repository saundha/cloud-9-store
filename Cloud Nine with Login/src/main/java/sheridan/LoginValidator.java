package sheridan;

public class LoginValidator {

	public static final String REGEX = "^[a-zA-Z][a-zA-z0-9]{5,}$";

	public static boolean isValidLoginName(String loginName) {
		return loginName.matches(REGEX);
	}
}
