package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginRegular() {
		assertTrue("Invalid login", LoginValidator.isValidLoginName("ramses"));
	}

	@Test(expected = NullPointerException.class)
	public void testIsValidLoginException() {
		assertFalse("Invalid login", LoginValidator.isValidLoginName(null));
	}

	@Test
	public void testIsValidLoginBoundryIn() {
		assertTrue("Invalid login", LoginValidator.isValidLoginName("s1aund"));
	}

	@Test
	public void testIsValidLoginBoundryOut() {
		assertFalse("Invalid login", LoginValidator.isValidLoginName("1saund"));
	}

	@Test
	public void testIsValidLoginLengthRegular() {
		assertTrue("Invalid login", LoginValidator.isValidLoginName("saundha"));
	}

	@Test(expected = NullPointerException.class)
	public void testIsValidLoginLengthException() {
		assertFalse("Invalid login", LoginValidator.isValidLoginName(null));
	}

	@Test
	public void testIsValidLoginLengthBoundryIn() {
		assertTrue("Invalid login", LoginValidator.isValidLoginName("saundh"));
	}

	@Test
	public void testIsValidLoginLengthBoundryOut() {
		assertFalse("Invalid login", LoginValidator.isValidLoginName("saund"));
	}

}
